package J05020;

public class SinhVien implements Comparable<SinhVien>{
    private String maSv;
    private String hoTen;
    private String lop;
    private String email;

    public SinhVien(String maSv, String hoTen, String lop, String email) {
        this.maSv = maSv;
        this.hoTen = hoTen;
        this.lop = lop;
        this.email = email;
    }

    @Override
    public String toString() {
        return maSv+" "+hoTen+" "+lop+" "+email;
    }

    @Override
    public int compareTo(SinhVien o) {
        if(lop.equals(o.lop)){
            return maSv.compareTo(o.maSv);
        }
        return lop.compareTo(o.lop);
    }
}
