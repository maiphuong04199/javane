import java.util.Scanner;

public class tn08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());

        for(int i = 1; i <= n; i++){
            String s = sc.nextLine();
            char[] arr = s.toCharArray();
            int j = arr.length-1;
            while(j >= 0){
                if(arr[j] == '0'){
                    arr[j] = '1';
                    break;
                }else{
                    arr[j] = '0';
                }
                j--;
            }
            if(j < 0){
                for(int k = 0; k < arr.length; k++)
                    System.out.print(0);
            }else{
                System.out.print(arr);
            }
            System.out.println();
        }
    }
}
