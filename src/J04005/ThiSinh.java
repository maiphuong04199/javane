package J04005;

public class ThiSinh {
    private String hoTen,ngaySinh;
    private float diem1,diem2,diem3,tong;

    public ThiSinh(String hoTen, String ngaySinh, float diem1, float diem2, float diem3) {
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.diem1 = diem1;
        this.diem2 = diem2;
        this.diem3 = diem3;
        tong = diem1+diem2+diem3;
    }

    @Override
    public String toString() {
        return hoTen+" "+ngaySinh+" "+String.format("%.1f",tong);
    }
}
