import java.util.Arrays;
import java.util.Scanner;

public class tn28 {
    static Scanner sc = new Scanner(System.in);
    static int test;
    static int n;
    static long first, second;
    static int[] a;

    public static void main(String[] args) {
        test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            first = 0;
            second = 0;
            a = new int[n + 5];
            for (int i = 1; i <= n; i++)
                a[i] = sc.nextInt();
            Arrays.sort(a, 1, n + 1);
            for (int i = 1; i <= n; i++)
                if (first * 10 + a[i] < second * 10 + a[i]) {
                    first = first * 10 + a[i];
                } else second = second * 10 + a[i];

            System.out.println(second + first);
        }
    }
}
