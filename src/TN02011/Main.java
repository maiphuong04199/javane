package TN02011;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        List<SanPham> list = new ArrayList<>();
        while (test-->0){
            list.add(new SanPham(sc.nextLine(),Integer.parseInt(sc.nextLine())));
        }
        Collections.sort(list);
        String s = sc.nextLine();
        for(SanPham sp : list)
            if(sp.getMa().charAt(0)==s.charAt(0))
                System.out.println(sp);
    }
}
