import java.util.Scanner;

public class DSA06025 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int step = 0;
        int arr[] = new int[n+5];
        for(int i = 1; i <= n; i++){
            arr[i] = sc.nextInt();
        }
        int j;
        System.out.println("Buoc 0: "+arr[1]);
        for(int i = 2; i <= n; i++){
            int key = arr[i];
            j = i - 1;
            while(j >= 0 && arr[j] > key){
                arr[j+1]  = arr[j];
                j = j - 1;
            }
            ++step;
            arr[j+1] = key;
            System.out.print("Buoc "+step+": ");
            for(int k = 1; k <= i; k++)
                System.out.print(arr[k]+" ");
            System.out.println();
        }
    }
}
