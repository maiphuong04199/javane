import java.util.Scanner;

public class t308 {
    static int[][] c;
    static int MOD = (int) (1e9 +7);
    public static void main(String[] args) {
        c = new int[1005][1005];
        for(int i = 0; i <= 1000; i++){
            for(int j = 0; j <= i; j++){
                if(i==j || j==0) c[i][j] =1;
                else c[i][j] = (c[i-1][j] + c[i-1][j-1]) % MOD;
            }
        }
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            int k = sc.nextInt();
            System.out.println(c[n][k]);
        }
    }
}
