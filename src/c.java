import java.util.Scanner;

public class c {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n= sc.nextInt();
        for(int i = 1; i <= n; i++){
            int a = sc.nextInt();
            int b = sc.nextInt();
            long result = ucln(a,b);
            System.out.println((long) a * b/result+" "+result);
        }
    }
    static long ucln(int a, int b){
        if(b==0)    return a;
        else return ucln(b,a%b);
    }
}
