package J04020;

public class Pair {
    private int a, b;

    public Pair(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public boolean isPrime() {
        return checkPrime(a) && checkPrime(b);
    }

    private boolean checkPrime(int n) {
        if (n < 2) return false;
        else for (int i = 2; i <= Math.sqrt(n); i++)
            if (n % i == 0) return false;
        return true;
    }

    @Override
    public String toString() {
        return a+" "+b;
    }
}
