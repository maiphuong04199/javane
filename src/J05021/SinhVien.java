package J05021;

public class SinhVien implements Comparable<SinhVien> {
    private String maSv, hoTen, lop, email;

    public SinhVien(String maSv, String hoTen, String lop, String email) {
        this.maSv = maSv;
        this.hoTen = hoTen;
        this.lop = lop;
        this.email = email;
    }

    @Override
    public String toString() {
        return maSv + " " + hoTen + " " + lop + " " + email;
    }

    @Override
    public int compareTo(SinhVien o) {
        return maSv.compareTo(o.maSv);
    }
}
