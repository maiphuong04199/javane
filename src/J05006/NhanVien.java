package J05006;

import java.util.Date;

public class NhanVien {
    private String maNv,hoTen,gioiTinh,ngaySinh,diaChi,maSoThue,ngayKyHopDong;

    public NhanVien(int id,String hoTen, String gioiTinh, String ngaySinh, String diaChi, String maSoThue, String ngayKyHopDong) {
        this.hoTen = hoTen;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
        this.maSoThue = maSoThue;
        this.ngayKyHopDong = ngayKyHopDong;
        this.maNv = String.format("%05d",id);
    }

    @Override
    public String toString() {
        return maNv+" "+hoTen+" "+gioiTinh+" "+ngaySinh+" "+diaChi+" "+maSoThue+" "+ngayKyHopDong;
    }
}
