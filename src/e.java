import java.util.Scanner;

public class e {
    public static void main(String[] args) {
        long fibo[] = new long[95];
        fibo[1] = 1;
        fibo[2] = 1;
        for(int i = 3; i <= 92; i++){
            fibo[i] = fibo[i-1] + fibo[i-2];
        }
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 1; i <= n; i++){
            int t = sc.nextInt();
            System.out.println(fibo[t]);
        }
    }
}
