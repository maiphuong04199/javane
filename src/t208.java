import java.util.Scanner;

public class t208 {
    static int n;
    static long MAX = 1000000007;

    static class MaTran {
        long[][] c;

        MaTran() {
            c = new long[2][2];
            c[0][0] = 0;
            c[0][1] = 1;
            c[1][0] = 1;
            c[1][1] = 1;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            MaTran a = new MaTran();
            a = power(a, n);
            System.out.println(a.c[0][1]);
        }
    }

    static MaTran mul(MaTran a, MaTran b) {
        MaTran x = new MaTran();
        for (int i = 0; i <= 1; i++) {
            for (int j = 0; j <= 1; j++) {
                x.c[i][j] = 0;
                for (int k = 0; k <= 1; k++)
                    x.c[i][j] = (x.c[i][j] + a.c[i][k] * b.c[k][j]) % MAX;
            }
        }
        return x;
    }

    static MaTran power(MaTran a, int n) {
        if (n == 1) return a;
        if (n % 2 == 0) {
            MaTran multi = power(a, n / 2);
            return mul(multi, multi);
        }
        return mul(power(a, n-1), a);
    }
}
