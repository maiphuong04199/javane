import java.util.Scanner;

public class t207 {
    static long[] fi;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        fi  = new long[94];
        fi[1] = 1; fi[2] = 1;
        for(int i = 3; i <= 92; i++)
            fi[i] = fi[i-1] + fi[i-2];
        while (test-- > 0) {
            int n = sc.nextInt();
            long i = sc.nextLong();
            System.out.println(Try(n,i));
        }
    }

    private static char Try(int n, long i) {
        if(n==1)    return 'A';
        else if(n==2)   return 'B';
        else if(i > fi[n-2]) return Try(n-1,i-fi[n-2]);
        return Try(n-2,i);
    }
}
