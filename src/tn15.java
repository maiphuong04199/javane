import java.util.Scanner;

public class tn15 {
    static int[] X = {1, 0};
    static int[] Y = {0, 1};
    static int[][] a;
    static boolean[][] dd;
    static int[] step;
    static boolean canComeToGoal = false;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            int n = sc.nextInt();
            a = new int[n + 5][n + 5];
            dd = new boolean[n + 5][n + 5];
            step = new int[2 * n + 5];
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    a[i][j] = sc.nextInt();
                    dd[i][j] = true;
                }
            }
            canComeToGoal = false;
            if(a[1][1] == 0){
                System.out.println(-1);
            }else{
                Try(1,1, n, 1);
                if(!canComeToGoal) System.out.println(-1);
                else System.out.println();
            }
        }
    }

    private static void Try(int x, int y, int n, int k) {
        for (int i = 0; i < 2; i++) {
            int nx = x + X[i];
            int ny = y + Y[i];
            step[k] = i;
            if (nx <= n && ny <= n && a[nx][ny] == 1 && dd[nx][ny]) {
                dd[nx][ny] = false;
                if (nx == n && ny == n) {
                    canComeToGoal = true;
                    for (int j = 1; j <= k; j++)
                        if(step[j] == 1) System.out.print("R");
                        else System.out.print("D");
                    System.out.print(" ");
                } else {
                    if (k <= 2 * n - 2)
                        Try(nx, ny, n, k + 1);
                }
                dd[nx][ny] = true;
            }
        }
    }
}
