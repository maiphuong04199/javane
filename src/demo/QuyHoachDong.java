package demo;


import java.util.Scanner;

/**
 * QUY HOẠCH ĐỘNG
 * Bài toán tổ hợp
 *
 *
 * Bài toán tối ưu
 */
public class QuyHoachDong {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
//        System.out.println(fibo(30));
        long[] fibonaci = new long[94];
        fibonaci[1] = 1;
        fibonaci[2] = 1;
        for(int i = 1; i <= 92; i++){
            fibonaci[i]  =fibonaci[i-1]+fibonaci[i-2];
        }
    }
    static long fibo(int n){
        if(n==0)    return 0;
        if(n==1)    return 1;
        return fibo(n-1)+fibo(n-2);
    }
}
