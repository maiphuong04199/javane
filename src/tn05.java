import java.util.Scanner;
import java.util.StringTokenizer;

public class tn05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n= sc.nextInt();
        sc.next();
        for(int i = 1; i <= n; i++){
            String s = sc.nextLine();
            s = s.trim();
            StringTokenizer stk = new StringTokenizer(s," ");
            while(stk.hasMoreTokens()){
                char[] k = stk.nextToken().toCharArray();
                for(int j = 0; j < k.length; j++){
                    if(j == 0){
                        if(k[j] >= 'a' && k[j] <= 'z'){
                            k[j]-=32;
                        }
                    }else{
                        if(k[j] >= 'A' && k[j] <= 'Z'){
                            k[j]+=32;
                        }
                    }
                }
                System.out.print(k);
                System.out.print(" ");
            }
            System.out.println("");
        }
    }
}
