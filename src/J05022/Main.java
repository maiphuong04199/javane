package J05022;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        List<SinhVien> list = new ArrayList<>();
        while (test-->0){
            list.add(new SinhVien(sc.nextLine(),sc.nextLine(),sc.nextLine(),sc.nextLine()));
        }
        int q = Integer.parseInt(sc.nextLine());
        while(q-->0){
            String lop = sc.nextLine();
            System.out.println("DANH SACH SINH VIEN LOP "+lop+":");
            for(SinhVien sv : list)
                if(sv.getLop().equals(lop))
                    System.out.println(sv);
        }
    }
}
