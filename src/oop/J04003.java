package oop;

public class J04003 {
    private long tu, mau;

    public J04003(long tu, long mau) {
        this.tu = tu;
        this.mau = mau;
    }

    @Override
    public String toString() {
        return tu + "/" + mau;
    }

    public void rutGon() {
        long x = ucln(tu, mau);
        tu /= x;
        mau /= x;
    }

    private long ucln(long a, long b) {
        if (b == 0) return a;
        return ucln(b, a % b);
    }
}
