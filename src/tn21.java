import java.util.Scanner;

public class tn21 {
    static int[] t, p, a;
    static int[][] value;
    static int maxValue;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            maxValue = 0;
            a = new int[13];
            t = new int[21];
            //i == j
            p = new int[21];
            value = new int[13][13];
            for (int i = 1; i <= 8; i++) {
                for (int j = 1; j <= 8; j++) {
                    value[i][j] = sc.nextInt();
                }
            }
            //i = n-j+1
            Try(1, 0);
            System.out.println(maxValue);
        }
    }

    static void Try(int k, int sumCur) {
        for (int i = 1; i <= 8; i++) {
            //hang
            if (t[i - k + 8] == 0 && p[i + k] == 0 && a[i] == 0) {
                t[i - k + 8] = 1;
                p[i + k] = 1;
                a[i] = 1;
                sumCur += value[i][k];
                if (k == 8) {
                    maxValue = Math.max(sumCur, maxValue);
                } else
                    Try(k + 1, sumCur);
                sumCur -= value[i][k];
                t[i - k + 8] = 0;
                p[i + k] = 0;
                a[i] = 0;
            }

        }

    }
}
