package J04021;

import java.util.TreeSet;

public class IntSet {
    private TreeSet<Integer> set = new TreeSet<>();

    public IntSet() {
    }

    public IntSet(int[] a) {
        for(int i = 0; i < a.length; i++)
            set.add(a[i]);
    }

    @Override
    public String toString() {
        String res = "";
        for(Integer i : set){
            res += i +" ";
        }
        return res;
    }

    public IntSet union(IntSet s2) {
        IntSet u = new IntSet();
        u.set.addAll(set);
        u.set.addAll(s2.set);
        return u;
    }
}
