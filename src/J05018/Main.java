package J05018;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        List<HocSinh> list = new ArrayList<>();
        for (int i = 1; i <= test; i++) {
            sc.nextLine();
            String name = sc.nextLine();
            float diemTb = 0.0F;
            diemTb += sc.nextFloat()*2;
            diemTb += sc.nextFloat()*2;
            for (int j = 1; j <= 8; j++)
                diemTb += sc.nextFloat();
            list.add(new HocSinh(i, name, diemTb));
        }
        Collections.sort(list);
        for (HocSinh hs : list)
            System.out.println(hs);
    }
}
