import java.util.*;

public class J02026 {
    static int[] x;
    static int[] a;
    static int[] c;
    static int n,k;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            k =  sc.nextInt();
            a = new int[n + 5];
            x = new int[n + 5];
            c = new int[n + 5];
            for (int i = 1; i <= n; i++)
                c[i] = sc.nextInt();
            Arrays.sort(c, 1, n + 1);
            for (int i = 1; i <= n; i++) a[i] = n-i+1;
            Try(1);
        }
    }

    static void Try(int x) {
        for(int i = a[x-1]+1; i <= n-k+x; i++){
            a[x] = i;
            if(x == k)  check();
            else Try(x+1);
        }
    }

    private static void check() {
        boolean isIncrease = true;
        for (int i = 1; i < k; i++)
            if (c[i + 1] < c[i]) {
                isIncrease = false;
                break;
            }
        if (isIncrease) for (int i = 1; i <= k; i++) System.out.print(c[a[i]] + " ");
        System.out.println();
    }

}
