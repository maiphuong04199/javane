import java.math.BigInteger;
import java.util.Scanner;

public class d {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 1; i <= n; i++){
            int a = sc.nextInt();
            BigInteger b = sc.nextBigInteger();
            BigInteger b1 = BigInteger.valueOf(a);
            BigInteger gcd = b1.gcd(b);
            System.out.println(gcd);
        }
    }
}
