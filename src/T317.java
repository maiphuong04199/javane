import java.util.Scanner;

public class T317 {
    static Scanner sc = new Scanner(System.in);
    static int test;
    static int n;
    public static void main(String[] args) {
        test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            int[] a = new int[n+5];
            for(int i = 0; i <= n; i++)
                a[i] = i;
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= Math.sqrt(i); j++){
                    a[i] = Math.min(a[i],a[i-j*j]+1);
                }
            }
            System.out.println(a[n]);
        }
    }
}
