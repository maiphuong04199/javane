import java.util.Scanner;

public class DSA06024 {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int n = sc.nextInt();
        int[] a = new int[n+5];
        for(int i = 1; i <= n; i++){
            a[i] = sc.nextInt();
        }
        int step = 1,min_idx,tmp;
        for(int i = 1; i < n; i++){
            min_idx = i;
            for(int j = i+1; j <= n; j++){
                if(a[j] < a[min_idx])
                    min_idx = j;
            }
            tmp = a[i];
            a[i] = a[min_idx];
            a[min_idx] = tmp;
            System.out.print("Buoc "+step+": ");
            for(int k = 1; k <= n; k++)
                System.out.print(a[k]+" ");
            step++;
            System.out.println();
        }
    }
}
