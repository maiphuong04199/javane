import java.util.Scanner;

public class t206 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            int n = sc.nextInt();
            long k = sc.nextLong();
            System.out.println(find(n, k));
        }
    }

    private static int find(int n, long k) {
        long x = (long) Math.pow(2, n - 1);
        if (k == x) return n;
        else if (k < x) return find(n - 1, k);
        return find(n - 1, k - x);
    }
}
