import java.util.Scanner;

public class f {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for(int i = 1; i <= n; i++){
            long t = sc.nextLong();
            if(checkPrime(t)) System.out.println(t);
            else{
                int result = 0;
                for(int j = 2; j <= Math.sqrt(t); j++){
                    if(t%j==0) {
                        if(checkPrime(j)){
                            result = Math.max(result,j);
                        }
                        if(checkPrime(t/j)){
                            result = (int) Math.max(result,t/j);
                        }
                    }
                }
                System.out.println(result);
            }
        }
    }
    static boolean checkPrime(long n){
        if(n<2) return false;
        else for(int i = 2; i <= Math.sqrt(n); i++)
            if(n%i==0)  return false;
        return true;
    }
}
