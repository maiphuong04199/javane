import java.util.Scanner;

public class tn26 {
    static int[] a;
    static int n, k, s, count;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            count = 0;
            n = sc.nextInt();
            k = sc.nextInt();
            s = sc.nextInt();
            a = new int[k+5];
            if (n == 0 && k == 0 && s == 0)
                return;
            Try(1);
            System.out.println(count);
        }
    }

    static void Try(int x) {
        for (int i = a[x - 1] + 1; i <= n - k + x; i++) {
            a[x] = i;
            if (x == k) {
                check();
            } else Try(x + 1);
        }
    }

    private static void check() {
        int sum = 0;
        for (int i = 1; i <= k; i++)
            sum += a[i];
        if (sum == s) {
            ++count;
        }
    }
}
