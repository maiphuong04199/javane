package J05024;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        List<SinhVien> list = new ArrayList<>();
        while(test-->0){
            list.add(new SinhVien(sc.nextLine(),sc.nextLine(),sc.nextLine(),sc.nextLine()));
        }
        int q = Integer.parseInt(sc.nextLine());
        while(q-->0){
            String nganh = sc.nextLine();
            System.out.println("DANH SACH SINH VIEN NGANH "+nganh.toUpperCase()+":");
            for(SinhVien sv : list)
            {
                if(sv.getNganh()!=null && sv.getNganh().getTen().equals(nganh))
                    System.out.println(sv);
            }
        }
    }
}
